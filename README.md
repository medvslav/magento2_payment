# Magento2_Payment
This is Magento 2 module for adding a payment method in the checkout (the user does not enter addition information into the form fields during checkout).

After installing this module in the admin panel, the new menu item will appear in the Stores -> Configuration -> Sales -> Payment Methods. Its name is "Paym".
You can enter the configuration of this payment module here.

The form has following fields:

- Enabled (for activating this payment method in the checkout process)

- Title

- New order status

- Payment from applicable countries

- Payment from Specific countries
#

- Instructions

- Sort Order

After installing this module you have to activate this payment method in the configuration.